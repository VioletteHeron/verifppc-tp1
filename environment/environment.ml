open Interval;;
open Abstract_syntax_tree;;

module Environment = (
  struct
    (* a map, with variables *)
    module VarMap = Mapext.Make(String)
    type env = Interval.itv VarMap.t
    type t =
      | Val of env
      | BOT
    let rec eval_int (m:env) (exp:int_expr) = ( match exp with
      | AST_identifier (id, _) -> VarMap.find id m (* chercher mon identifiant dans m et retourner l'intervalle associé  varmap find id m *)
      | AST_int_const (const,_) -> Itv((Int(Z.of_string const),Int(Z.of_string const)))
      | AST_int_unary (op, (e, _)) -> (match op with
        | AST_UNARY_PLUS -> eval_int m e
        | AST_UNARY_MINUS -> Interval.neg (eval_int m e)
      )
      | AST_int_binary (op, (e,_), (f,_)) -> (match op with
        (* arithmetic operators, work only for int *)
        | AST_PLUS          -> Interval.add (eval_int m e) (eval_int m f)
        | AST_MINUS         -> Interval.sub (eval_int m e) (eval_int m f)
        | AST_MULTIPLY      -> Interval.mul (eval_int m e) (eval_int m f)
        | AST_DIVIDE        -> Interval.div (eval_int m e) (eval_int m f)

      )
    )
    let rec eval_bool (m:env) (op:compare_op) (e:int_expr) (f:int_expr) = ( match op with
      | AST_EQUAL         -> Interval.make_eq (eval_int m e) (eval_int m f)  (* e == e *)
      | AST_NOT_EQUAL     -> Interval.make_neq (eval_int m e) (eval_int m f)  (* e != e *)
      | AST_LESS          -> Interval.make_lt (eval_int m e) (eval_int m f)  (* e < e *)
      | AST_LESS_EQUAL    -> Interval.make_lteq (eval_int m e) (eval_int m f)  (* e <= e *)
      | AST_GREATER       -> Interval.make_gt (eval_int m e) (eval_int m f)  (* e > e *)
      | AST_GREATER_EQUAL -> Interval.make_gteq (eval_int m e) (eval_int m f)  (* e >= e *)
    )

    let rec eval_stat (m:env) (s:stat) = ( match s with
      | AST_assign ((id, _), (e, _))                  -> VarMap.add id (eval_int m e) m (* find the variable in the env and replace it by the eval of e *)
      | AST_block exprs                               -> List.fold_left (fun m2 (e2, _) -> (eval_stat m2 e2)) m exprs (* loop on the expressions and eval each of them *)
      | AST_if ((bool_expr, _), (thn, _), None)       -> if (match bool_expr with
                                                            AST_compare (operator, (exp1, _), (exp2, _)) -> let x,y = (eval_bool m operator exp1 exp2) in x<>BOT && y<>BOT)
                                                        then (eval_stat m thn)
                                                        else m (* else you need to reduce the variable's domain *)
		  | AST_if ((bool_expr, _), (thn, _), Some (els, _)) -> if (match bool_expr with
                                                              AST_compare (operator, (exp1, _), (exp2, _)) -> let x,y = (eval_bool m operator exp1 exp2) in
                                                              x<>BOT && y<>BOT)
                                                          then (eval_stat m thn)
                                                          else (eval_stat m els) (* if it is true then eval the then otherwise eval the else *)
      | AST_while ((condition, _), (body, _))         -> if (match condition with
                                                              AST_compare (operator, (exp1, _), (exp2, _)) -> let x,y = (eval_bool m operator exp1 exp2) in
                                                              x<>BOT && y<>BOT)
                                                          then let m2 = (eval_stat m body) in (eval_stat m2 body) (* recursivity *)
                                                          else m
      | AST_assert (a, _)                             -> m

      | AST_print values                              -> m
      | AST_PRINT_ALL                                 -> m
      | AST_HALT                                      -> m
    )


  end
);;

(*
File "main.ml", line 20, characters 19-23:
Error: This expression has type
         Abstract_syntax_tree.prog =
           Abstract_syntax_tree.toplevel list * Abstract_syntax_tree.extent
       but an expression was expected of type Abstract_syntax_tree.stat
Makefile:126: recipe for target 'main.cmo' failed
*)

(* (AST_BINARY_PLUS, (AST_identifier ("x",_),_), (AST_identifier ("y",_),_)) *)
