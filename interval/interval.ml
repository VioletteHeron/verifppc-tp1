  module Bound = (
    struct
      type t =
        | Int of Z.t
        | Pinf
        | Minf
      let tostr a = match a with
        | Pinf -> "+oo"
        | Minf -> "-oo"
        | Int(b) -> Z.to_string b
      let add a b = match (a,b) with
        | _,Pinf | Pinf,_ -> Pinf
        | _,Minf | Minf,_ -> Minf
        | Int(c), Int(d) -> Int(Z.add c d)
      let neg a = match a with
        | Pinf -> Minf
        | Minf -> Pinf
        | Int(b) -> Int(Z.mul (Z.minus_one) b)
      let sub a b = match (a,b) with
        | _,Pinf | Pinf,_ -> Pinf
        | _,Minf | Minf,_ -> Minf
        | Int(c), Int(d) -> Int(Z.sub c d)
      let mul a b = match a,b with
        | Int(a),_ | _,Int(a) when a=Z.zero -> Int(a)
        | Int(c),Pinf | Pinf,Int(c) -> if c > Z.zero then Pinf else Minf
        | Int(c),Minf | Minf,Int(c) -> if c > Z.zero then Minf else Pinf
        | Pinf,Minf | Minf,Pinf -> Pinf
        | Minf, _ | _, Minf -> Minf
        | Pinf, _ | _, Pinf -> Pinf
        | Int(c), Int(d) -> Int(Z.mul c d)
      let min a b = match a,b with
        | Int(c),Pinf | Pinf,Int(c) -> Int(c)
        | _,Minf | Minf,_ -> Minf
        | Pinf,Pinf -> Pinf
        | Int(c), Int(d) -> Int(Z.min c d)
      let max a b = match a,b with
        | _,Pinf | Pinf,_ -> Pinf
        | Int(c),Minf | Minf,Int(c) -> Int(c)
        | Minf,Minf -> Minf
        | Int(c), Int(d) -> Int(Z.max c d)
      let div a b = match a,b with
        | Int(x),Int(y) when y=Z.zero -> if (x > Z.zero) then Pinf else Minf
        | Pinf,Pinf | Minf,Minf -> Pinf
        | Pinf,Minf | Minf,Pinf -> Minf
        | _,Pinf | _,Minf -> Int(Z.zero)
        | Int(x),Int(y) -> Int(Z.div x y)
        | _ -> Int(Z.zero)
      let gteq a b = match a,b with
        | Minf,_ | _,Pinf -> false
        | Pinf,_ | _,Minf -> true
        | Int(a),Int(b) -> a >= b
      let lteq a b = (gteq b a)
      let gt a b = a <> b && gteq a b
      let lt a b = a <> b && lteq a b
      end
  )

  (* note : (Bound.Pinf,Bound.Minf) is temporarly BOT*)
  module Interval = (
    struct
      type bound = Bound.t
      type itv =
        | Itv of bound * bound
        | BOT
      (*MATCH ALL WITH BOT*)
      (*MUST RETURN INTERVALS INSTEAD OF BOUND*BOUND*)
      let neg x = match x with
        | BOT -> BOT
        | Itv(a,b) -> Itv( (Bound.neg b), (Bound.neg a))
      let add x y  = match x,y with
        | n,BOT | BOT,n -> n
        | Itv(a,b),Itv(c,d) -> (Itv ((Bound.add a c), (Bound.add b d)))
      let sub x y = match x,y with
        | n,BOT -> n
        | BOT,n -> (neg n)
        | Itv(a,b),Itv(c,d) -> (Itv ((Bound.sub a d), (Bound.sub b c)))
      let mul x y = match x,y with
        | BOT,_ | _,BOT -> BOT
        | Itv(a,b),Itv(c,d) -> (Itv ((Bound.min (Bound.min (Bound.mul a c) (Bound.mul a d)) (Bound.min (Bound.mul b c) (Bound.mul b d))),
                                (Bound.max (Bound.max (Bound.mul a c) (Bound.mul a d)) (Bound.max (Bound.mul b c) (Bound.mul b d)))))
      let div x y =  match x,y with
        | BOT,_ | _,BOT -> BOT
        | Itv(a,b),Itv(c,d) -> (Itv ((Bound.min (Bound.min (Bound.div a c) (Bound.div a d)) (Bound.min (Bound.div b c) (Bound.div b d))),
                                (Bound.max (Bound.max (Bound.div a c) (Bound.div a d)) (Bound.max (Bound.div b c) (Bound.div b d)))))
      let subset x y =  match x,y with
        | _,BOT -> false
        | BOT,n -> true
        | Itv(a,b),Itv(c,d) -> (Bound.gteq a  c) && (Bound.lteq b d)
      let join x y = match x,y with
        | BOT,n | n,BOT -> n
        | Itv(a,b),Itv(c,d) -> (Itv ((Bound.min a c), (Bound.max b d)))
      let meet x y = match x,y with
        | BOT,_ | _,BOT -> BOT
        | Itv(a,b),Itv(c,d) -> if (Bound.lt b c) then BOT else (Itv ((Bound.max a c), (Bound.min b d)))
      let widen x y = match x,y with
        | n,BOT -> n
        | BOT,_ -> (Itv (Bound.Minf, Bound.Pinf))
        | Itv(a,b),Itv(c,d) -> (Itv ((if (Bound.lteq a c) then Bound.Minf else a), (if (Bound.gteq b d) then b else Bound.Pinf)))
      let narrow x y = match x,y with
        | BOT,_ -> BOT
        | n,BOT -> n
        | Itv(a,b),Itv(c,d) -> (Itv ((if (a = Bound.Minf) then c else a), (if (b = Bound.Pinf) then d else b)))
      let print_inter i = match i with
        | BOT -> (Format.printf "BOT\n")
        | Itv (a, b) -> (Format.printf "[%s,%s]\n" (Bound.tostr a) (Bound.tostr b))
      let make_lteq x y = match x,y with
        | BOT,_ | _,BOT -> BOT,BOT
        | Itv(a,b),Itv(c,d) ->
          if (Bound.lteq a (Bound.min b d))
          then (if (Bound.lteq (Bound.max a c) d)
            then (Itv(a,(Bound.min b d))),(Itv((Bound.max a c),d))
            else BOT,BOT )
          else BOT,BOT
      let make_lt x y =match x,y with
        | BOT,_ | _,BOT -> BOT,BOT
        | Itv(a,b),Itv(c,d) ->
          if (Bound.lteq a (Bound.min b (Bound.sub d (Bound.Int(Z.one)))) )
          then (if (Bound.lteq (Bound.max c (Bound.add a (Bound.Int(Z.one)))) d)
            then Itv(a,(Bound.min b (Bound.sub d (Bound.Int(Z.one))))),Itv((Bound.max c (Bound.add a (Bound.Int(Z.one)))),d)
            else BOT,BOT)
          else BOT,BOT
      let make_gteq x y = let y1,x1 = (make_lteq y x) in x1,y1
      let make_gt x y = let y1,x1 = (make_lt y x) in x1,y1
      let make_eq x y = let x1,x2 = (make_lteq x y) in let x3,x4 = (make_gteq x y) in (join x1 x3),(join x2 x4)
      let make_neq x y = let x1,x2 = (make_lt x y) in let x3,x4 = (make_gt x y) in (join x1 x3),(join x2 x4)
    end
  )
