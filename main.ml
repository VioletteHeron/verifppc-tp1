(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2014
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Simple driver: parses the file given as argument and prints it back.

  You should modify this file to call your functions instead!
*)

open Interval;;
open Environment

(* parse and print filename *)
let doit filename =
  let prog = File_parser.parse_file filename in
  Abstract_syntax_printer.print_prog Format.std_formatter prog


(* parses arguments to get filename *)
let main () =
  match Array.to_list Sys.argv with
  | _::filename::_ -> doit filename
  | _ -> invalid_arg "no source file specified"

let _ = main ();;

(* addition *)
Interval.print_inter (Interval.add (Itv (Int(Z.of_int 0),Int(Z.of_int 1))) (Itv (Int(Z.of_int 2),Int(Z.of_int 2))));;
Interval.print_inter (Interval.add (Itv (Bound.Minf,Int(Z.of_int 1))) (Itv (Int(Z.of_int 2),Int(Z.of_int 2))));;
Interval.print_inter (Interval.add (Itv (Int(Z.of_int 0),Bound.Pinf)) (Itv (Bound.Minf,Int(Z.of_int 2))));;
Interval.print_inter (Interval.add (Itv (Int(Z.of_int 0),Bound.Pinf)) Interval.BOT);;
Interval.print_inter (Interval.add Interval.BOT (Itv (Bound.Minf,Int(Z.of_int 2))));;

(* substraction *)
Interval.print_inter (Interval.sub (Itv (Int(Z.of_int 10),Int(Z.of_int 11))) (Itv (Int(Z.of_int 0),Int(Z.of_int 2))));;
Interval.print_inter (Interval.sub (Itv (Int(Z.of_int (-6)),Int(Z.of_int 15))) (Itv (Int(Z.of_int 2),Int(Z.of_int 4))));;
Interval.print_inter (Interval.sub (Itv (Int(Z.of_int (-6)),Int(Z.of_int 15))) Interval.BOT);;
Interval.print_inter (Interval.sub Interval.BOT (Itv (Int(Z.of_int 2),Int(Z.of_int 4))));;
Interval.print_inter (Interval.sub Interval.BOT Interval.BOT);;

(* multiplication *)
Interval.print_inter (Interval.mul (Itv (Int(Z.of_int (-2)),Int(Z.of_int 5))) (Itv (Int(Z.of_int (-2)),Int(Z.of_int 4))));;
Interval.print_inter (Interval.mul (Itv (Int(Z.of_int (-2)),Int(Z.of_int 5))) (Itv (Int(Z.of_int (-2)),Bound.Pinf)));;
Interval.print_inter (Interval.mul (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Bound.Minf,Int(Z.of_int 4))));;
Interval.print_inter (Interval.mul (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) Interval.BOT);;
Interval.print_inter (Interval.mul Interval.BOT (Itv (Bound.Minf,Int(Z.of_int 4))));;
Interval.print_inter (Interval.mul Interval.BOT Interval.BOT);;

(* division *)
Interval.print_inter (Interval.div (Itv (Int(Z.of_int (-2)),Int(Z.of_int 6))) (Itv (Int(Z.of_int 2),Int(Z.of_int 4))));;
Interval.print_inter (Interval.div (Itv (Int(Z.of_int (-2)),Int(Z.of_int 6))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;

(* subset *)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;  (*true*)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 0),Int(Z.of_int 3))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;  (*true*)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 1),Int(Z.of_int 4))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;  (*true*)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 1),Int(Z.of_int 4))) (Itv (Bound.Minf,Bound.Pinf)));;  (*true*)
Format.printf "%b\n" (Interval.subset Interval.BOT (Itv (Bound.Minf,Bound.Pinf)));;  (*true*)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;  (*false*)
Format.printf "%b\n" (Interval.subset (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) Interval.BOT);;  (*false*)
Format.printf "%b\n" (Interval.subset (Itv (Bound.Minf,Int(Z.of_int 5))) (Itv (Int(Z.of_int 0),Int(Z.of_int 4))));;  (*false*)

(* join *)
Interval.print_inter (Interval.join (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Bound.Minf,Int(Z.of_int 4))));;
Interval.print_inter (Interval.join (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Int(Z.of_int 4),Bound.Pinf)));;
Interval.print_inter (Interval.join (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Int(Z.of_int 4),Int(Z.of_int 8))));;
Interval.print_inter (Interval.join (Itv (Int(Z.of_int (-2)),Int(Z.of_int 4))) (Itv (Int(Z.of_int 6),Int(Z.of_int 10))));;
Interval.print_inter (Interval.join (Itv (Bound.Minf,Bound.Pinf)) (Itv (Bound.Minf,Bound.Pinf)));;
Interval.print_inter (Interval.join (Itv (Int(Z.of_int (-2)),Int(Z.of_int 4))) Interval.BOT);;
Interval.print_inter (Interval.join Interval.BOT (Itv (Int(Z.of_int 6),Int(Z.of_int 10))));;
Interval.print_inter (Interval.join Interval.BOT Interval.BOT);;

(* meet *)
Interval.print_inter (Interval.meet (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Bound.Minf,Int(Z.of_int 4))));;
Interval.print_inter (Interval.meet (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Int(Z.of_int 4),Bound.Pinf)));;
Interval.print_inter (Interval.meet (Itv (Int(Z.of_int 2),Int(Z.of_int 5))) (Itv (Int(Z.of_int 4),Int(Z.of_int 8))));;
Interval.print_inter (Interval.meet (Itv (Int(Z.of_int (-2)),Int(Z.of_int 4))) (Itv (Int(Z.of_int 6),Int(Z.of_int 10))));;
Interval.print_inter (Interval.meet (Itv (Bound.Minf,Bound.Pinf)) (Itv (Bound.Minf,Bound.Pinf)));;
Interval.print_inter (Interval.meet (Itv (Int(Z.of_int (-2)),Int(Z.of_int 4))) Interval.BOT);;
Interval.print_inter (Interval.meet Interval.BOT (Itv (Int(Z.of_int 6),Int(Z.of_int 10))));;
Interval.print_inter (Interval.meet Interval.BOT Interval.BOT);;

(* widen *)
Interval.print_inter (Interval.widen (Itv (Int(Z.of_int 6),Int(Z.of_int 10))) (Itv (Int(Z.of_int 6),Int(Z.of_int 8))));;
Interval.print_inter (Interval.widen (Itv (Int(Z.of_int 6),Int(Z.of_int 10))) (Itv (Int(Z.of_int 6),Int(Z.of_int 20))));;
Interval.print_inter (Interval.widen (Itv (Int(Z.of_int 2),Int(Z.of_int 10))) (Itv (Int(Z.of_int 3),Int(Z.of_int 8))));;
Interval.print_inter (Interval.widen Interval.BOT (Itv (Int(Z.of_int 3),Int(Z.of_int 20))));;
Interval.print_inter (Interval.widen (Itv (Int(Z.of_int 2),Int(Z.of_int 10))) Interval.BOT);;

(* narrow *)
Interval.print_inter (Interval.narrow (Itv (Bound.Minf,Int(Z.of_int 10))) (Itv (Int(Z.of_int 6),Int(Z.of_int 8))));;
Interval.print_inter (Interval.narrow (Itv (Int(Z.of_int 6),Int(Z.of_int 10))) (Itv (Int(Z.of_int 7),Bound.Pinf)));;
Interval.print_inter (Interval.narrow (Itv (Int(Z.of_int 6),Bound.Pinf)) (Itv (Int(Z.of_int 7),Bound.Pinf)));;
Interval.print_inter (Interval.narrow (Itv (Int(Z.of_int 6),Int(Z.of_int 10))) (Itv (Int(Z.of_int 6),Int(Z.of_int 8))));;

(* make lteq *)
Format.printf "make lteq\n";;
let x,y = (Interval.make_lteq (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 4),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_lteq (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 3),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_lteq (Itv (Int(Z.of_int 4),Int(Z.of_int 6))) (Itv (Int(Z.of_int 1),Int(Z.of_int 3)))) in
Interval.print_inter x ;
Interval.print_inter y;;

(* make gteq *)
Format.printf "make gteq\n";;
let x,y = (Interval.make_gteq (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 4),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_gteq (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 3),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_gteq (Itv (Int(Z.of_int 4),Int(Z.of_int 6))) (Itv (Int(Z.of_int 1),Int(Z.of_int 3)))) in
Interval.print_inter x ;
Interval.print_inter y;;


(* make lt *)
Format.printf "make lt\n";;
let x,y = (Interval.make_lt (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 4),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_lt (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 3),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_lt (Itv (Int(Z.of_int 4),Int(Z.of_int 6))) (Itv (Int(Z.of_int 1),Int(Z.of_int 3)))) in
Interval.print_inter x ;
Interval.print_inter y;;

(* make gt *)

Format.printf "make gt\n";;
let x,y = (Interval.make_gt (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 4),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_gt (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 3),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_gt (Itv (Int(Z.of_int 4),Int(Z.of_int 6))) (Itv (Int(Z.of_int 1),Int(Z.of_int 3)))) in
Interval.print_inter x ;
Interval.print_inter y;;

(* make equal *)
Format.printf "make eq\n";;
let x,y = (Interval.make_eq (Itv (Int(Z.of_int 1),Int(Z.of_int 3))) (Itv (Int(Z.of_int 4),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_eq (Itv (Int(Z.of_int 1),Int(Z.of_int 5))) (Itv (Int(Z.of_int 3),Int(Z.of_int 6)))) in
Interval.print_inter x ;
Interval.print_inter y;;

let x,y = (Interval.make_eq (Itv (Int(Z.of_int 4),Int(Z.of_int 6))) (Itv (Int(Z.of_int 1),Int(Z.of_int 3)))) in
Interval.print_inter x ;
Interval.print_inter y;;

(* make not equal *)
Format.printf "make neq\n";;
let x,y = (Interval.make_neq (Itv (Int(Z.of_int 1),Int(Z.of_int 1))) (Itv (Int(Z.of_int 1),Int(Z.of_int 5)))) in
Interval.print_inter x ;
Interval.print_inter y;;
let x,y = (Interval.make_neq (Itv (Int(Z.of_int 3),Int(Z.of_int 3))) (Itv (Int(Z.of_int 1),Int(Z.of_int 5)))) in
Interval.print_inter x ;
Interval.print_inter y;;
let x,y = (Interval.make_neq (Itv (Int(Z.of_int 5),Int(Z.of_int 5))) (Itv (Int(Z.of_int 1),Int(Z.of_int 5)))) in
Interval.print_inter x ;
Interval.print_inter y;;
